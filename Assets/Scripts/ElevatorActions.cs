﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorActions : MonoBehaviour
{

    /* A altura dos andares seguem um padrão,
    a diferença entre cada um deles é y=2.51, ou seja
    assim que apertar o botão: 
    Novolimite = PosicaoAtualDoElevador + 2.51*/
    public double novoLimiteSuperior; // Variavel para forçar o elevador parar em cada Andar na subida
    public double novoLimiteInferior; // Variavel para forçar o elevador parar em cada andar na descida
    public double alturaMaxima; // O elevador não subirá mais que esta altura
    public double alturaMinima; // O elevador não descerá mais que esta altura
    public double alturaAtualElevador; // Altura atual do Elevador
    ElevatorButton Botao;
    public int andar;
    public bool topo;
    public bool terreo;

    

    // Start is called before the first frame update
    void Start()
    {
        novoLimiteSuperior = this.transform.position.y + 2.51;
        novoLimiteInferior = this.transform.position.y;
        alturaMinima = this.transform.position.y;
        alturaMaxima = 7.54;
    }

    // Update is called once per frame
    void Update()
    {
        alturaAtualElevador = this.transform.position.y;
        /*
        Debug.Log("SUBINDO:: alturaAtual <= alturaMaxima: "+(alturaAtual<=alturaMaxima));
        Debug.Log("SUBINDO:: alturaAtual <= novoLimiteSuperior: "+(alturaAtual<=novoLimiteSuperior));
        Debug.Log("DESCENDO:: alturaAtual == alturaMaxima: "+(alturaAtual==alturaMaxima));
        Debug.Log("DESCENDO:: alturaAtual >= novoLimiteInferior: "+(alturaAtual>=novoLimiteInferior));
        Debug.Log("DESCENDO:: alturaAtual >= alturaMinima: "+(alturaAtual>=alturaMinima));
       
        Debug.Log("AlturaElevador == AlturaMaxima?? "+(alturaAtualElevador==alturaMaxima));
        Debug.Log("AlturaElevador <= AlturaMaxima?? "+(alturaAtualElevador<=alturaMaxima));
        Debug.Log("AlturaElevador >= AlturaMaxima?? "+(alturaAtualElevador>=alturaMaxima));
        Debug.Log("ALTURÃ MAXIMA "+alturaMaxima);
        Debug.Log("Altura Atual do Elevador: "+alturaAtualElevador);
        */
        Debug.Log("ALTURA MAXIMA "+alturaMaxima);
        Debug.Log("Altura Do elevador "+alturaAtualElevador);
        Debug.Log("Novo Limite Superior: "+novoLimiteSuperior);
        Debug.Log("Novo Limite Inferior: "+novoLimiteInferior);
        Debug.Log("Andar: "+andar);
        Debug.Log("Topo? "+topo);
        Debug.Log("Terreo? "+terreo);
        
    }

    void OnTriggerStay(Collider other)
    {
        Botao = gameObject.GetComponentInChildren<ElevatorButton>();
        // Subir
        if (other.gameObject.tag == "Player" && Botao.ButtonActived) {
            Debug.Log("Voce esta no "+andar+"th Andar");
            if (alturaAtualElevador <= alturaMaxima && andar!=3) {
                if (alturaAtualElevador <= novoLimiteSuperior) {
                    this.transform.position += new Vector3(0.0f, 0.01f, 0.0f);
                } else {
                    novoLimiteSuperior += 2.51;
                    Botao.ButtonActived=false;
                    ++andar;

                }

            // Descer Tudo
           } else if (alturaAtualElevador > alturaMinima) {                              
               if (alturaAtualElevador >= novoLimiteInferior) {
                   this.transform.position += new Vector3(0.0f, -0.01f, 0.0f);
               } 
               else {
                   novoLimiteInferior -= 2.51;
                   Botao.ButtonActived=false;
                   --andar;
               }
               
           } else {
               ;
           }

          
        }

    }


}
