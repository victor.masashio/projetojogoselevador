﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour
{
    public Vector3 PlayerDeslocamento;
    Vector3 PlayerInitialPosition;

    // Start is called before the first frame update
    void Start()
    {
        PlayerInitialPosition = this.transform.position;
        //Debug.Log("position of Player is "+ pos);
    }

    // Update is called once per frame
    void Update()
    {
        // Se cair no limbo, retornar a posicao inicial
        if (this.transform.position.y < 0) {
            this.transform.position = PlayerInitialPosition;
        }
        //Debug.Log("Altura atual do Player: "+this.transform.position.y);
    }

    
}
