Matéria:

    Programação de Jogos

Integrantes:

    Victor Masashi,
    Vinicius Canuto


HOWTOPLAY:

    O jogador se movimenta utilizando (W A S D).
    
    Para ativar o elevador, o jogador precisa chegar perto do botão no Elevador 
    e apertar 'E' para ativa-lo.

    OBS: 
    O elevador na subida vai parar em cada andar, para ele continuar subindo, é necessário
    apertar novamente o Botão. Quando estiver no Topo, ao apertar o botão do elevador novamente,
    ele irá descer até o Térreo(onde o jogo se inicia) diretamente sem nenhuma parada nos Andares.
